package com.bunhann.retrofitgetdata;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "subjects")
public class Subject extends Model {

    @PrimaryKey
    @ColumnInfo( name = "id")
    @SerializedName("id")
    @Expose
    private Integer subjectID;

    @ColumnInfo( name = "subject_title")
    @SerializedName("subject_title")
    @Expose
    private String subjectTitle;

    public Subject() {
    }

    public Integer getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(Integer subjectID) {
        this.subjectID = subjectID;
    }

    public String getSubjectTitle() {
        return subjectTitle;
    }

    public void setSubjectTitle(String subjectTitle) {
        this.subjectTitle = subjectTitle;
    }

    @Override
    public String toString() {
        return this.getSubjectTitle();
    }
}
