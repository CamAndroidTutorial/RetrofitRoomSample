package com.bunhann.retrofitgetdata;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStudentActivity extends AppCompatActivity{

    private EditText edName;
    private EditText edAge;
    private Spinner spGender;
    private Button btnSend;
    private Button btnDelete;
    private DataService client;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.student_activity);

        edName =  (EditText) findViewById(R.id.edName);
        edAge =  (EditText) findViewById(R.id.edGender);
        spGender =  (Spinner) findViewById(R.id.spinner);
        btnSend =  (Button) findViewById(R.id.btnSend);
        btnDelete =  (Button) findViewById(R.id.btnDelete);

        client = RestClient.getClient().create(DataService.class);

        final Student student = (Student) getIntent().getParcelableExtra("STUDENT");
        if (student!=null){
            edName.setText(student.getStudentName());
            edAge.setText(student.getStudentAge().toString());
            if (student.getStudentGender().toLowerCase()=="male"){
                spGender.setSelection(0);
            } else spGender.setSelection(1);

            btnSend.setText("Update");
            btnDelete.setVisibility(View.VISIBLE);
        }

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student s = new Student();
                s.setStudentName(edName.getText().toString());
                s.setStudentAge(Integer.parseInt(edAge.getText().toString()));
                s.setStudentGender(spGender.getSelectedItem().toString());

                if (student!=null){
                    // Update Student
                    client.updateStudent(student.getStudentId(), s).enqueue(new Callback<Student>() {
                        @Override
                        public void onResponse(Call<Student> call, Response<Student> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(AddStudentActivity.this, "Updated ", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("ERROR", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<Student> call, Throwable t) {

                        }
                    });

                } else {

                    //Insert Student
                    client.createUser(s).enqueue(new Callback<Student>() {
                        @Override
                        public void onResponse(Call<Student> call, Response<Student> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(AddStudentActivity.this, "Inserted " + response.body().toString(), Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("ERROR", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<Student> call, Throwable t) {
                            Log.e("Failure", t.getMessage());
                        }
                    });
                }


            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.deleteStudent(student.getStudentId()).enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {
                        if (response.isSuccessful()){
                            Toast.makeText(AddStudentActivity.this, "Deleted! " , Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {
                        Log.e("Failure", t.getMessage());
                    }
                });
            }
        });


    }
}
