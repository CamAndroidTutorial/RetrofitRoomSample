package com.bunhann.retrofitgetdata;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface SubjectDAO {

    @Query("SELECT * FROM subjects")
    List<Subject> getAllSubjects();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSubject(Subject... subjects);


}