package com.bunhann.retrofitgetdata;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface StudentDAO {

    @Query("SELECT * FROM students")
    List<Student> getStudents();

    @Query("SELECT * FROM students where id= :student_id")
    Student getStudent(int student_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStudent(Student... students);

    @Update
    void updateStudent(Student student);

    @Delete
    void deleteStudent(Student student);



}
