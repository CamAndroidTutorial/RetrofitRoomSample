package com.bunhann.retrofitgetdata;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Student.class, Subject.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static AppDatabase INSTANCE;

    public abstract StudentDAO studentDAO();
    public abstract SubjectDAO subjectDAO();

    public static AppDatabase getAppDB(Context context){
        if (INSTANCE == null){
            INSTANCE =
                    Room.databaseBuilder(context, AppDatabase.class,
                            "school.db")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();

        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
