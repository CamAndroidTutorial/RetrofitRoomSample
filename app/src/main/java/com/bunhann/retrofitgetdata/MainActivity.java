package com.bunhann.retrofitgetdata;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerStudent;
    private ArrayList<Student> studentList;
    private ArrayList<Subject> subjectList;

    private StudentAdapter adapter;

    private DataService client;
    AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerStudent = (RecyclerView) findViewById(R.id.recyclerStudent);

        studentList = new ArrayList<>();

        appDatabase = AppDatabase.getAppDB(this);

        appDatabase.getOpenHelper().getWritableDatabase();

        //Get Data from WebService and Save to Database
        //final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        client = RestClient.getClient().create(DataService.class);


        //Get All Students
        client.getAllStudents().enqueue(new Callback<ArrayList<Student>>() {
            @Override
            public void onResponse(Call<ArrayList<Student>> call, Response<ArrayList<Student>> response) {
                if (response.isSuccessful()){
                    studentList = response.body();
                    Student arr[] = new Student[studentList.size()];
                    for (int i=0;i<arr.length;i++){
                        arr[i] = studentList.get(i);

                        Log.d("STUDENT ", arr[i].toString());
                    }
                    InsertToDBRoom insertToDBRoom = new InsertToDBRoom();
                    insertToDBRoom.execute(arr);
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Student>> call, Throwable t) {

            }
        });

        //Get AllSubject
        client.getAllSubjects().enqueue(new Callback<ArrayList<Subject>>() {
            @Override
            public void onResponse(Call<ArrayList<Subject>> call, Response<ArrayList<Subject>> response) {
                if (response.isSuccessful()){
                    subjectList = response.body();
                    Subject arr[] = new Subject[subjectList.size()];
                    for (int i=0;i<arr.length;i++){
                        arr[i] = subjectList.get(i);
                        Log.d("SUBJECT", arr[i].toString());
                    }
                    InsertToDBRoom insertToDBRoom = new InsertToDBRoom();
                    insertToDBRoom.execute(arr);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Subject>> call, Throwable t) {

            }
        });


//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Calendar cal = Calendar.getInstance();
//        String currentDate = "2018-09-02 01:00:00";
//        //Get New Students Only base on Created_at
//        client.getNewStudents(currentDate).enqueue(new Callback<ArrayList<Student>>() {
//            @Override
//            public void onResponse(Call<ArrayList<Student>> call, Response<ArrayList<Student>> response) {
//                if (response.isSuccessful()){
//                    studentList = response.body();
//                    Student arr[] = new Student[studentList.size()];
//                    for (int i=0;i<arr.length;i++){
//                        arr[i] = studentList.get(i);
//                    }
//                    InsertToDBRoom insertToDBRoom = new InsertToDBRoom();
//                    insertToDBRoom.execute(arr);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ArrayList<Student>> call, Throwable t) {
//
//            }
//        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.menuAdd:

                Intent intent = new Intent(this, AddStudentActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {



        recyclerStudent.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerStudent, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Student s = studentList.get(position);
                Toast.makeText(MainActivity.this, s.getStudentId().toString(), Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(view.getContext(), AddStudentActivity.class);
                intent.putExtra("STUDENT", s);
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        super.onResume();
    }




    public class InsertToDBRoom extends AsyncTask<Model, Void, Void>{


//        @Override
//        protected Void doInBackground(Student... students) {
////            for (int i=0; i<students.length; i++){
////                Student student = new Student();
////                student.setStudentId(students[i].getStudentId());
////                student.setStudentName(students[i].getStudentName());
////                student.setStudentGender(students[i].getStudentGender());
////                student.setStudentAge(students[i].getStudentAge());
////                appDatabase.studentDAO().insertStudent(student);
////            }
//            appDatabase.studentDAO().insertStudent(students);
//            return null;
//        }
//        @Override
//        protected Void doInBackground(List<Student>... lists) {
//            for (int i=0; i<lists.length;i++){
//                Student student = new Student();
//                student.setStudentId(lists[i].get(0).getStudentId());
//                student.setStudentName(lists[i].get(0).getStudentName());
//                student.setStudentGender(lists[i].get(0).getStudentGender());
//                student.setStudentAge(lists[i].get(0).getStudentAge());
//                appDatabase.studentDAO().insertStudent(student);
//            }
//
//            return null;
//        }

        @Override
        protected Void doInBackground(Model... models) {
            if (models instanceof Student[]){
                appDatabase.studentDAO().insertStudent((Student[]) models);
            } else if (models instanceof Subject[]){
                appDatabase.subjectDAO().insertSubject((Subject[]) models);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Toast.makeText(MainActivity.this, "Insert Completed!", Toast.LENGTH_SHORT).show();

            adapter = new StudentAdapter(getBaseContext(), studentList);
            recyclerStudent.setAdapter(adapter);
            recyclerStudent.setLayoutManager(new LinearLayoutManager(getBaseContext()));

            super.onPostExecute(aVoid);
        }
    }

}
